<?php
//2 nieuwste berichten ophalen, behalve diegene die aan het begin moet staan (instelbaar op pagina 'home')
$eerstebericht = get_field('field_53cf90bf71f80');
$nieuwsberichten = new WP_Query(array('posts_per_page' => 2,'post__not_in' => array($eerstebericht)));
get_header();
?>
<div id="slider">
	<ul id="slides">
		<li>
			<figure>
				<?php echo get_the_post_thumbnail($eerstebericht, 'home-slider'); ?>	
				<figcaption><?php the_field('field_53cf6bdddac6e', $eerstebericht); ?><a href="<?php echo get_permalink($eerstebericht); ?>" class="readmore">read more</a></figcaption>
			</figure>
		</li>
		<?php if($nieuwsberichten->have_posts()): ?>
		<?php while($nieuwsberichten->have_posts()): ?>
		<?php $nieuwsberichten->the_post(); ?>
		<li>
			<figure>
				<?php the_post_thumbnail( 'home-slider'); ?>	
				<figcaption><?php the_field('field_53cf6bdddac6e'); ?><a href="<?php the_permalink(); ?>" class="readmore">read more</a></figcaption>
			</figure>
		</li>
		<?php endwhile; ?>
		<?php endif; ?>
		<?php wp_reset_postdata(); ?>
	</ul>
	<nav id="sliderpager">
		<ul>
			<li><a href="#<?php echo $eerstebericht; ?>"><?php echo get_the_title($eerstebericht); ?></a></li>
			<?php if($nieuwsberichten->have_posts()): ?>
			<?php while($nieuwsberichten->have_posts()): ?>
			<?php $nieuwsberichten->the_post(); ?>
			<li><a href="#<?php the_id(); ?>"><?php the_title(); ?></a></li>
			<?php endwhile; ?>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
			<li><a href="<?php echo get_post_type_archive_link('product'); ?>"><span>Need help?</span> Use our problem solver</a></li>
		</ul>
	</nav>

	<section id="wynnsinfo">
		<?php if(get_field('field_53cfa07d1bd6d')): ?><h2><?php the_field('field_53cfa07d1bd6d'); ?></h2><?php endif; ?>
		<?php if(get_field('field_53cfa0ac113fd')): ?><p><?php the_field('field_53cfa0ac113fd'); ?></p><?php endif; ?>
		<div id="essentialfluids">
			<?php if(get_field('field_53cfa0b7113fe')): ?><p><?php the_field('field_53cfa0b7113fe'); ?></p><?php endif; ?>
			<ul>
				<li id="fuel"><a href="#">Fuel</a></li>
				<li id="oil"><a href="#">Oil</a></li>
				<li id="coolant"><a href="#">Coolant</a></li>
			</ul>
		</div>
	</section>

	<section id="additivesinfo">
		<article id="additives">
			<?php the_field('field_53cfa13fa1818'); ?>
			<?php $uitgelichtprods = get_field('field_53cfa27fa7201'); ?>
			<nav id="uitgelichtepager">
				<ul>
				<?php foreach ($uitgelichtprods as $post): ?>
					<?php setup_postdata($post); ?>
					<li><a href=""></a></li>
				<?php endforeach; ?>		
				</ul>
			</nav>
			<ul id="uitgelichteproducten">
				<?php foreach ($uitgelichtprods as $post): ?>
					<?php setup_postdata($post); ?>
					<?php the_title(); ?>
				<?php endforeach; ?>		
			</ul>
			<?php wp_reset_postdata(); ?>
		</article>
	</section>

</div>
<?php get_footer(); ?>