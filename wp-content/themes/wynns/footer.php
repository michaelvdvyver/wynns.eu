    <footer>
        <div class="column">
            <a href="<?php bloginfo('home'); ?>" id="footerlogo"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/footerlogo.svg" alt="<?php bloginfo('name'); ?>"></a>
            <h3><?php bloginfo('name'); ?></h3>
            <address>
                <?php if(get_field('field_53ce70307b5c4', 'option')){ the_field('field_53ce70307b5c4', 'option'); } ?> <?php if(get_field('field_53ce70497b5c5', 'option')){ the_field('field_53ce70497b5c5', 'option'); } ?><br />
                <?php if(get_field('field_53ce705e7b5c6', 'option')){ the_field('field_53ce705e7b5c6', 'option'); } ?> <?php if(get_field('field_53ce70767b5c7', 'option')){ the_field('field_53ce70767b5c7', 'option'); } ?> - <?php if(get_field('field_53ce70867b5c8', 'option')){ the_field('field_53ce70867b5c8', 'option'); } ?>
            </address>
        </div>
        <div class="column">
            <strong>General Information</strong>
            <a href="" id="footermail-general"></a>
            <strong>Technical Information</strong>
            <a href="" id="footermail-technical"></a>
        </div>
        <div class="column">
            <strong>Sales Information</strong>
            <a href="" id="footermail-sales"></a>
            <strong>Health &amp; Evironment</strong>
            <a href="" id="footermail-health"></a>
        </div>
        <div class="column">
            <p>
                &copy; Copyright <?php echo date("Y"); ?> Wynn's. All Rights Reserved.<br />
                <a href="http://www.commefort.com" target="_blank">design &amp; development: commefort</a>
            </p>
        </div>        
    </footer>
    <?php wp_footer(); ?>
    <?php if(get_field('field_53ce744b0405c', 'option')): ?>
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
        function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
        e=o.createElement(i);r=o.getElementsByTagName(i)[0];
        e.src='//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
        ga('create','<?php the_field("field_53ce744b0405c", "option"); ?>');ga('send','pageview');
    </script>
    <?php endif; ?>
</body>
</html>