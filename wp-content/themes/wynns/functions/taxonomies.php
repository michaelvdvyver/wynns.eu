<?php
add_action( 'init', 'register_taxonomy_doelgroep' );

function register_taxonomy_doelgroep() {

    $labels = array( 
        'name' => __( 'Doelgroepen', 'wynns' ),
        'singular_name' => __( 'Doelgroep', 'wynns' ),
        'search_items' => __( 'Doelgroepen Zoeken', 'wynns' ),
        'popular_items' => __( 'Populaire Doelgroepen', 'wynns' ),
        'all_items' => __( 'Alle Doelgroepen', 'wynns' ),
        'parent_item' => __( 'Parent Doelgroep', 'wynns' ),
        'parent_item_colon' => __( 'Parent Doelgroep:', 'wynns' ),
        'edit_item' => __( 'Pas Doelgroep Aan', 'wynns' ),
        'update_item' => __( 'Update Doelgroep', 'wynns' ),
        'add_new_item' => __( 'Voeg Nieuwe Doelgroep Toe', 'wynns' ),
        'new_item_name' => __( 'Nieuwe Doelgroep', 'wynns' ),
        'separate_items_with_commas' => __( 'Separate doelgroepen with commas', 'wynns' ),
        'add_or_remove_items' => __( 'Verwijder Of Voeg Doelgroepen Toe', 'wynns' ),
        'choose_from_most_used' => __( 'Kies uit de meest gebruikte Doelgroepen', 'wynns' ),
        'menu_name' => __( 'Doelgroepen', 'wynns' ),
    );

    $args = array( 
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'show_admin_column' => false,
        'hierarchical' => true,
        'rewrite' => array( 'slug' => 'producten' ),
        'query_var' => true
    );

    register_taxonomy( 'doelgroep', array('product'), $args );
}