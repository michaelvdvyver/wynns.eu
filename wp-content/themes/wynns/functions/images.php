<?php
//add support for thumbnails
add_theme_support( 'post-thumbnails' );

//image sizes
add_image_size( 'full-width', 1500, 9999, false );
add_image_size( 'home-slider', 1500, 480, false );