<?php
//custom post types
function cpt_product() {

	$labels = array(
		'name'                => __( 'Producten', 'Post Type General Name', 'wynns' ),
		'singular_name'       => __( 'Product', 'Post Type Singular Name', 'wynns' ),
		'menu_name'           => __( 'Producten', 'wynns' ),
		'parent_item_colon'   => __( 'Hoofd Product:', 'wynns' ),
		'all_items'           => __( 'Alle Producten', 'wynns' ),
		'view_item'           => __( 'Bekijk Product', 'wynns' ),
		'add_new_item'        => __( 'Voeg Nieuw Product Toe', 'wynns' ),
		'add_new'             => __( 'Nieuw Toevoegen', 'wynns' ),
		'edit_item'           => __( 'Product Aanpassen', 'wynns' ),
		'update_item'         => __( 'Update Product', 'wynns' ),
		'search_items'        => __( 'Zoek Producten', 'wynns' ),
		'not_found'           => __( 'Niets Gevonden', 'wynns' ),
		'not_found_in_trash'  => __( 'Niets Gevonden In De Prullenmand', 'wynns' ),
	);
	$args = array(
		'label'               => __( 'product', 'wynns' ),
		'description'         => __( "Wynn's Producten", 'wynns' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail' ),
		'taxonomies'          => array( 'category' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'menu_icon'    		  => 'dashicons-cart'
	);
	register_post_type( 'product', $args );

}
add_action( 'init', 'cpt_product', 0 );

function ctp_dealer() {

	$labels = array(
		'name'                => __( 'Dealers', 'Post Type General Name', 'wynns' ),
		'singular_name'       => __( 'Dealer', 'Post Type Singular Name', 'wynns' ),
		'menu_name'           => __( 'Dealers', 'wynns' ),
		'parent_item_colon'   => __( 'Hoofd Dealer:', 'wynns' ),
		'all_items'           => __( 'Alle Dealers', 'wynns' ),
		'view_item'           => __( 'Bekijk Dealer', 'wynns' ),
		'add_new_item'        => __( 'Voege Nieuwe Dealer Toe', 'wynns' ),
		'add_new'             => __( 'Nieuwe Toevoegen', 'wynns' ),
		'edit_item'           => __( 'Dealer Aanpassen', 'wynns' ),
		'update_item'         => __( 'Update Dealer', 'wynns' ),
		'search_items'        => __( 'Zoek Dealers', 'wynns' ),
		'not_found'           => __( 'Niets Gevonden', 'wynns' ),
		'not_found_in_trash'  => __( 'Niets Gevonden In De Prullenmand', 'wynns' ),
	);
	$args = array(
		'label'               => __( 'dealer', 'wynns' ),
		'description'         => __( 'Wynn\'s dealers', 'wynns' ),
		'labels'              => $labels,
		'supports'            => array( 'title' ),
		'taxonomies'          => array( 'country' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => false,
		'capability_type'     => 'page',
		'menu_icon'    		  => 'dashicons-location-alt'
	);
	register_post_type( 'dealer', $args );

}

add_action( 'init', 'ctp_dealer', 0 );

function cpt_problem() {

	$labels = array(
		'name'                => __( 'Problemen', 'Post Type General Name', 'wynns' ),
		'singular_name'       => __( 'Probleem', 'Post Type Singular Name', 'wynns' ),
		'menu_name'           => __( 'Problemen', 'wynns' ),
		'parent_item_colon'   => __( 'Hoofd Probleem:', 'wynns' ),
		'all_items'           => __( 'Alle Problemen', 'wynns' ),
		'view_item'           => __( 'Bekijk Probleem', 'wynns' ),
		'add_new_item'        => __( 'Voeg Nieuw Probleem Toe', 'wynns' ),
		'add_new'             => __( 'Nieuw Toevoegen', 'wynns' ),
		'edit_item'           => __( 'Probleem Aanpassen', 'wynns' ),
		'update_item'         => __( 'Update Probleem', 'wynns' ),
		'search_items'        => __( 'Zoek Problemen', 'wynns' ),
		'not_found'           => __( 'Niets Gevonden', 'wynns' ),
		'not_found_in_trash'  => __( 'Niets Gevonden In De Prullenmand', 'wynns' ),
	);
	$args = array(
		'label'               => __( 'probleem', 'wynns' ),
		'description'         => __( 'Alle problemen en hun oplossingen', 'wynns' ),
		'labels'              => $labels,
		'supports'            => array( 'title' ),
		'taxonomies'          => array( 'category' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => false,
		'capability_type'     => 'page',
		'menu_icon'    		  => 'dashicons-lightbulb'
	);
	register_post_type( 'probleem', $args );

}
add_action( 'init', 'cpt_problem', 0 );

add_filter( 'enter_title_here', 'change_title_placeholder' );

function change_title_placeholder( $input ) {
    global $post_type;

    if ( is_admin() && 'dealer' == $post_type )
        return __( 'Vul de naam van de dealer in', 'wynns' );

    return $input;
}
