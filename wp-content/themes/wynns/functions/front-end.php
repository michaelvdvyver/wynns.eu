<?php
//scripts and styles
function scripts_styles() {
	//modernizr		
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js', array(), '1.1.0', false );
	//main css
	wp_enqueue_style( 'style', get_stylesheet_uri(), array(), '0.0.1' );
	//plugins js
	wp_enqueue_script( 'plugins', get_template_directory_uri() . '/js/plugins.js', array('jquery'), '0.0.1', true );
	//main js
	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jquery', 'plugins'), '0.0.1', true );
}
add_action( 'wp_enqueue_scripts', 'scripts_styles' );

//menu's
function register_menus() {
  register_nav_menu('hoofdmenu',__( 'Hoofdmenu' ));
}
add_action( 'init', 'register_menus' );

//returns <title> and <meta> tags
function titleandmeta(){
	$favicondir = get_stylesheet_directory_uri() . '/img/favicons/';
	if(is_front_page()){
		$title = '';	
		$description = '';
	} elseif(is_category()) {
		$title = '';	
		$description = '';
	} elseif(is_single()){
		$title = '';
		$description = '';
	} elseif(is_page()){
		$title = '';	
		$description = '';
	}
	$returndata = '<title>' . $title . '</title>
    <meta name="description" content="' . $description . '">
    <meta property="og:title" content="' . $title . '" />
    <meta property="og:site_name" content="' . get_bloginfo('name') .'" />
    <meta property="og:url" content="' . get_permalink() . '" />
    <meta property="og:description" content="' . $description . '" />
    <meta property="og:image" content="" />
    <meta property="og:app_id" content="1454030298179526" />
    <link rel="apple-touch-icon" sizes="57x57" href="' . $favicondir . '/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="' . $favicondir . '/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="' . $favicondir . '/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="' . $favicondir . '/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="' . $favicondir . '/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="' . $favicondir . '/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="' . $favicondir . '/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="' . $favicondir . '/apple-touch-icon-152x152.png">
	<link rel="icon" type="image/png" href="' . $favicondir . '/favicon-196x196.png" sizes="196x196">
	<link rel="icon" type="image/png" href="' . $favicondir . '/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="' . $favicondir . '/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="' . $favicondir . '/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="' . $favicondir . '/favicon-32x32.png" sizes="32x32">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/mstile-144x144.png">';
	echo $returndata;
}