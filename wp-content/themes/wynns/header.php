<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php titleandmeta(); ?>
        <?php wp_head(); ?>
    </head>
    <body>
    	<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1454030298179526&version=v2.0";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
    	<header>
    		<nav>
    			<ul>
    				<li><a href="#">Wynn's info</a></li>
    				<li><a href="#">Dealer login</a></li>
    			</ul>
    		</nav>
    		<a href="<?php bloginfo('home'); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.svg" alt="<?php bloginfo('name'); ?>"></a>
    		<?php wp_nav_menu( array( 'theme_location' => 'hoofdmenu', 'container' => 'nav' ) ); ?>
    		<nav>
    			<ul>
    				<li><a href="#" class="eu-en">EN</a></li>
    				<li><a href="#" class="be-nl">NL</a></li>
    				<li><a href="#" class="be-fr">FR</a></li>
    				<li><a href="#" class="nl-nl">NL</a></li>
    			</ul>
    		</nav>
    		<?php $doelgroepen = get_terms('doelgroep', array('hide_empty' => false)); ?>
    		<nav>
    			<ul>
    				<?php foreach ($doelgroepen as $doelgroep): ?>
    				<?php $doelgroeplink = get_term_link($doelgroep); ?>
    				<li><a href="<?php echo esc_url($doelgroeplink); ?>"><?php echo $doelgroep->name; ?></a></li>
    				<?php endforeach; ?>
    			</ul>
    		</nav>
    	</header>