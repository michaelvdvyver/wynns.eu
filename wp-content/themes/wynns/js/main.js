jQuery(document).ready(function( $ ) {
	//todo at init
	obscuremails();

	//functions
	//array of vars, put them together to build email for footer
	function obscuremails(){
		var emailgeneral = ["fo", "in", 64, "ns.eu", "wyn"];
		$('#footermail-general').html(emailgeneral[1] + emailgeneral[0] + String.fromCharCode(emailgeneral[2]) + emailgeneral[4] + emailgeneral[3]);
		$('#footermail-general').attr('href', 'mailto:' + emailgeneral[1] + emailgeneral[0] + String.fromCharCode(emailgeneral[2]) + emailgeneral[4] + emailgeneral[3]);
		var emailtechnical = ["cal", "techni", 64, "ns.eu", "wyn"];
		$('#footermail-technical').html(emailtechnical[1] + emailtechnical[0] + String.fromCharCode(emailtechnical[2]) + emailtechnical[4] + emailtechnical[3]);
		$('#footermail-technical').attr('href', 'mailto:' + emailtechnical[1] + emailtechnical[0] + String.fromCharCode(emailtechnical[2]) + emailtechnical[4] + emailtechnical[3]);
		var emailsales = ["les", "sa", 64, "ns.eu", "wyn"];
		$('#footermail-sales').html(emailsales[1] + emailsales[0] + String.fromCharCode(emailsales[2]) + emailsales[4] + emailsales[3]);
		$('#footermail-sales').attr('href', 'mailto:' + emailsales[1] + emailsales[0] + String.fromCharCode(emailsales[2]) + emailsales[4] + emailsales[3]);
		var emailhealth = ["ds", "ms", 64, "ns.eu", "wyn"];
		$('#footermail-health').html(emailhealth[1] + emailhealth[0] + String.fromCharCode(emailhealth[2]) + emailhealth[4] + emailhealth[3]);	
		$('#footermail-health').attr('href', 'mailto:' + emailhealth[1] + emailhealth[0] + String.fromCharCode(emailhealth[2]) + emailhealth[4] + emailhealth[3]);		
	}
});