<?php
include 'functions/custom-post-types.php';
include 'functions/front-end.php';
include 'functions/taxonomies.php';
include 'functions/images.php';

//acf options page
if(function_exists('acf_add_options_page')){
	acf_add_options_page();
}