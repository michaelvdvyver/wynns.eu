<?php get_header(); ?>
<?php if(have_posts()): ?>
	<?php while(have_posts()): ?>
	<?php the_post(); ?>
	<article>
		<h2><?php the_title(); ?></h2>
		<?php the_content(); ?>
		<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div>
	</article>
	<?php endwhile; ?>
<?php else: ?>
<?php endif; ?>
<?php get_footer(); ?>